1)SELECT DISTINCT maker,model
FROM product
WHERE type='PC' AND NOT EXISTS(
	SELECT model
    FROM pc
    WHERE product.model=model
);
2)SELECT maker,model
FROM product
WHERE type='PC' AND EXISTS(
	SELECT model
    FROM pc
    WHERE product.model=pc.model AND speed>=750 
);
3)SELECT maker,model
FROM product
WHERE type='PC' AND (EXISTS(
	SELECT model
    FROM pc
    WHERE product.model=pc.model AND speed>=750)
	) OR
    (EXISTS(
    SELECT model
    FROM laptop
    WHERE product.model=laptop.model AND speed>=750)
    )
;
4)SELECT maker,model,type
FROM product
WHERE model=(
SELECT model
FROM pc
WHERE price=(SELECT MAX(price) FROM pc)
)
AND
model IN(
SELECT model
FROM product
)

;
5)SELECT name,launched,displacement
FROM classes JOIN ships
WHERE classes.class=ships.class 
AND (classes.displacement>35
AND ships.launched>1922)
6)SELECT class,result,battle
FROM outcomes JOIN ships
WHERE outcomes.ship = ships.name AND EXISTS(
SELECT class
FROM outcomes
WHERE outcomes.result='sunk') 
7)SELECT DISTINCT maker 
FROM product,printer
WHERE printer.model=product.model AND product.maker = ANY(
SELECT maker 
FROM product,laptop
WHERE product.model = laptop.model);

8)SELECT maker 
FROM product,laptop
WHERE product.model = laptop.model
AND NOT product.maker = ANY(
SELECT maker 
FROM product,printer
WHERE printer.model=product.model
);
